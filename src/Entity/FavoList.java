package Entity;

public class FavoList {
	int count;
	String artistName;
	String trackTitle;
	int downloadDate;

public void setTheCount(int counter){
	count = counter;
}
public void setTheName(String name){
	artistName = name;
}
public void setTheTitle(String title){
	trackTitle = title;
}
public void setTheDate(int date){
	downloadDate = date;
}

public int getTheCount(){
	return count;
}
public String getTheName(){
	return artistName;
}
public String getTheTitle(){
	return trackTitle;
}
public int getTheDate(){
	return downloadDate;
}

}

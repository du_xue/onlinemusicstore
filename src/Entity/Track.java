package Entity;

public class Track {
	String artistName;
	String trackTitle;
	String trackUrl;
	int trackid;
public void setTrackID(int id){
	trackid = id;
}
public void setTheName(String name){
	artistName = name;
}
public void setTheTitle(String title){
	trackTitle = title;
}
public void setTheUrl(String url){
	trackUrl = url;
}

public String getTheName(){
	return artistName;
}
public String getTheTitle(){
	return trackTitle;
}
public String getTheUrl(){
	return trackUrl;
}
public int getTheID(){
	return trackid;
}
}

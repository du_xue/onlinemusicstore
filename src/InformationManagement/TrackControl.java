package InformationManagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Entity.Track;
public class TrackControl {
public List<Track> getTheTracks() throws SQLException{
	List<Track> trackArray = new ArrayList<Track>();
	 ResultSet rs = null;
           try
             {
        	   Class.forName("org.postgresql.Driver");
        	   Connection connection = null;
        	   connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DuXue","postgres", "900412");
               Statement st = connection.createStatement();
               String sql = "select trackid, tracktitle, trackurl,artistname from track order by trackid asc";
               rs = st.executeQuery(sql);
               while(rs.next()){
               	Track track = new Track();
               	track.setTrackID(rs.getInt(1));
               	track.setTheTitle(rs.getString(2));
               	track.setTheName(rs.getString(4));
               	track.setTheUrl(rs.getString(3));
               	trackArray.add(track);
               	track.getTheName();
               	track.getTheTitle();
               	track.getTheUrl();
               	track.getTheID();
         	   //
               }
             }
             catch (Exception ee)
             {
               // System.out.print("");
             }
       	finally{
       		rs.close();
       	
       	}
       	return trackArray;
       }
// search the name 
public List<Track> searchTheArtistName(String artistname) throws SQLException{
	List<Track> trackArray = new ArrayList<Track>();
	
	ResultSet rs = null;
	
	try{
		Class.forName("org.postgresql.Driver");
 	   Connection connection = null;
 	   connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DuXue","postgres", "900412");
        Statement st = connection.createStatement();
        String sql = "select  trackid, tracktitle, trackurl,artistname  from track  where artistname = '"+artistname+"'";
        rs = st.executeQuery(sql);
        while(rs.next()){
        	Track track = new Track();
        	track.setTrackID(rs.getInt(1));
        	track.setTheTitle(rs.getString(2));
        	track.setTheName(rs.getString(4));
        	track.setTheUrl(rs.getString(3));
        	trackArray.add(track);
        	track.getTheName();
        	track.getTheTitle();
        	track.getTheUrl();
        	track.getTheID();
  	   //
        }
      }
      catch (Exception ee)
      {
        // System.out.print("Wrong!");
      }
	finally{
		rs.close();
	
	}
	return trackArray;
}
//search the title 
public List<Track> searchTheTrackTitle(String tracktitle) throws SQLException{
	List<Track> trackArray = new ArrayList<Track>();
	
	ResultSet rs = null;

	try{
		Class.forName("org.postgresql.Driver");
 	    Connection connection = null;
 	    connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DuXue","postgres", "900412");
        Statement st = connection.createStatement();
        String sql = "select trackid, tracktitle, trackurl,artistname from track  where tracktitle = '"+tracktitle+"'";
        rs = st.executeQuery(sql);
        while(rs.next()){
        	Track track = new Track();
        	track.setTheTitle(rs.getString(2));
        	track.setTheName(rs.getString(4));
        	track.setTrackID(rs.getInt(1));
        	track.setTheUrl(rs.getString(3));
        	trackArray.add(track);
        	track.getTheName();
        	track.getTheTitle();
        	track.getTheUrl();
        	track.getTheID();
        	
        }
  	   //connection.close();
      }
      catch (Exception ee)
      {
         //System.out.print("Wrong!");
      }
	finally{
		rs.close();
	}
	return trackArray;
}

//add the track
public int addTheTrack(int id, String title, String url, String name){
	try{
		Class.forName("org.postgresql.Driver");
 	    Connection connection = null;
 	    connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DuXue","postgres", "900412");
        Statement st = connection.createStatement();
        String s="INSERT INTO track (trackid,  tracktitle, trackurl, artistname) VALUES("+id+",'"+ title +"','"+url+"','"+name+"')";
       st.executeUpdate(s);
        
      }
      catch (Exception ee)
      {
         //System.out.print("Wrong!");
      }
	finally{
	
	}
	return 1;
}
//delete the track
public int deleteTheTrack(int id){
	try{
		Class.forName("org.postgresql.Driver");
 	    Connection connection = null;
 	    connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DuXue","postgres", "900412");
        Statement st = connection.createStatement();
        String sql="delete from track where trackid="+id+"";
        st.executeUpdate(sql);
        
      }
      catch (Exception ee)
      {
        // System.out.print("Wrong!");
      }
	finally{
	
	}
	return 1;
}
//update the favorite list.

//**************************
//**************************
}
